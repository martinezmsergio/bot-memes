#-*- coding: utf-8 -*-
from telegram.ext import Updater
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext.dispatcher import run_async
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import ChatAction
import requests, json 
import random
import os



numFoto = 8000
def start(bot, update):
	""" This function will be executed when '/start' command is received """
	mensaje_start = ("Utiliza el comando '/send_meme' para recibir un meme aleatorio")
	bot.send_message(chat_id=update.message.chat_id, text=mensaje_start)

# ctrl+k+c comenta en bloque
# ctrl+k+u descomenta
# file_id = update.message.photo[-1].file_id #Obtiene el id del tamaño de foto mas grande disponible
# newFile = bot.get_file(file_id)
# nombre_foto = "user_" + numFoto
# numFoto = numFoto + 1
# newFile.download("./Imgs/usuarios/" + nombre_foto)
#photo = Filters.photo

def save_photo(bot, update):
	""" this function will be executed when a photo file is received """

	file_id = update.message.photo[-1].file_id
	newFile = bot.get_file(file_id)

	global numFoto
	nombre_foto = "user_" + str(numFoto) + ".jpg"
	numFoto = int(numFoto) + 1
	F=open("/home/pi/Desktop/bot/bot-memes/numero.txt","w")
	F.write(str(numFoto))
	F.close()

	#Descarga la foto 
	#newFile.download("./Imgs/usuarios/" + nombre_foto)
	newFile.download("/media/pi/usbExt4/imgsMemes/" + nombre_foto)
	msg = "La foto ha sido guardada correctamente"
	bot.send_message(chat_id=update.message.chat_id, text=msg)

def send_meme(bot, update):
	""" This function will be executed when '/send_meme' command is received """

#	bot.send_chat_action(chat_id=chat_id, action=ChatAction.UPLOAD_PHOTO)
	#Guarda el número de la foto en "numero.txt"

	directory = "/media/pi/usbExt4/imgsMemes/"

	random_image = random.choice(os.listdir(directory))
	photo_to_send = directory + '/' + random_image
	bot.sendPhoto(chat_id=update.message.chat_id, photo=open(photo_to_send, 'rb'))



def main(bot_token):
	""" Main function of the bot """
	updater = Updater(token=bot_token)
	dispatcher = updater.dispatcher


	F=open("/home/pi/Desktop/bot/bot-memes/numero.txt","r")
	global numFoto
	numFoto = F.read()
	F.close()

	# Command handlers
	start_handler = CommandHandler('start', start)
	send_meme_handler = CommandHandler('send_meme', send_meme)
	save_photo_handler = MessageHandler(Filters.photo, save_photo)

	# Add the handlers to the bot
	dispatcher.add_handler(start_handler)
	dispatcher.add_handler(send_meme_handler)
	dispatcher.add_handler(save_photo_handler)

	# Starting the bot
	updater.start_polling(allowed_updates=[])

if __name__ == "__main__":
	main(os.environ["token_bot_memes"])

