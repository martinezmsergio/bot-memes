# BOT-MEMES
Bot de Telegram que envía fotos de memes aleatorias y guarda automáticamente aquellos que se le pasen

# Pruebas 
El bot está actualmente activo en [Telegram](https://t.me/random_meme_bot)

# Uso
'/start' - Inicia el bot

'/send_meme' - Envía un meme aleatorio

Enviar una foto para que la guarde y pueda ser enviada posteriormente

# License
Este bot utiliza una licencia GPLv3 - Ver [LICENSE](https://gitlab.com/martinezmsergio/bot-memes/blob/master/LICENSE) para más detalles
